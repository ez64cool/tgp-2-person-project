require 'Game'
require 'StartScreen'

GS_Start, GS_Game = 0, 1

GameState = GS_Start

function love.load(arg)
  if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
  
  io.stdout:setvbuf("no")
  
  startSceenLoad(arg)
end

function love.keypressed(key)
  if(GameState == GS_Start) then
    
  elseif (GameState == GS_Game) then
    GameKeypressed(key)
  end
end

function love.mousepressed(x, y, button, istouch)
  if(GameState == GS_Start) then
    startScreenMousepressed(x, y, button, istouch)
    if(IsStartDone()) then
      GameState = GS_Game
      GameLoad()
    end
  elseif (GameState == GS_Game) then
    
  end
end

function love.update(dt)
  if(GameState == GS_Start) then
    startSceenUpdate(dt)
  elseif (GameState == GS_Game) then
    GameUpdate(dt)
  end
end

function love.draw()
  if(GameState == GS_Start) then
    startSceenDraw()
  elseif (GameState == GS_Game) then
    GameDraw()
  end
end