require 'Classes/GameObject'
require 'Classes/Bullet'

Turret = GameObject:new{
  bullets = {},
  elapsedTime = 0
}

function Turret:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Turret:InitTurret(x, y, w, h)
  self:Init(x, y, w, h, 4, "static")
  
  self.body:setFixedRotation(false)
  self.fixture:setSensor(true)
end

function Turret:Shoot(x, y)
  local temp = Bullet:new()
  local oX, oY = self.body:getPosition()
  temp:Init(oX, oY, 5, 5, 5, "dynamic")
  temp.body:setLinearVelocity(x, y)
  table.insert(self.bullets, temp)
  print("Shoot")
end

function Turret:Update(dt)
  self.elapsedTime = self.elapsedTime + dt
  local x = worldSetup.player.body:getX() - self.body:getX()
  local y = worldSetup.player.body:getY() - self.body:getY()
  local playerAngle = math.atan2(y, x)
  self.body:setAngle(playerAngle)
  if(self.elapsedTime > 2) then
    self:Shoot(x, y)
    self.elapsedTime = 0
  end
end

function Turret:Draw()
  for i, b in ipairs(self.bullets) do
    b:Draw()
  end
  
  love.graphics.setColor(72, 160, 14)
  if(self.frameCount > 0) then
    local x, y, w, h = self.frames[1]:getViewport();
    x, y = self.body:getPosition()
    love.graphics.draw(spriteSheet, self.frames[1], x, y, 0, 1, 1, w/2, h/2)
  else
    love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints()))
  end
end