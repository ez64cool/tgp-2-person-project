require 'Classes/Character'
require 'characterCreation'
require 'Globals'

Player = Character:new
{
  ableToJump = true,
  deltaTimeAnims = 0,
  moveLeft = true
}

function Player:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Player:InitPlayer()
  self.headNum = gHeadNum
  self.bodyNum = gBodyNum
  self.legNum = gLegNum
  self.red = gRed
  self.green = gGreen
  self.blue = gBlue
end

function Player:keypressed(key)
  if(key == "up" and self.ableToJump) then
    AnimationCheck(4, self.moveLeft)
    self.body:applyLinearImpulse(0, -1000)
    self.ableToJump = false;
  end
end

function Player:BeginContact(other, coll)
  local nx, ny = coll:getNormal()
  
  if(nx ~= 0) then
    self.body:applyLinearImpulse(nx * 1000, 0)
  end
  
  self.ableToJump = true;
  
  print("Normal X: " .. nx .. " Y: " .. ny)
end

function Player:EndContact(other, coll)
end

function Player:Update(dt)
  self.deltaTimeAnims = self.deltaTimeAnims + (dt * 10)
  if self.deltaTimeAnims > 2 then
    self.deltaTimeAnims = 0
  end
  
  if(love.keyboard.isDown("right")) then
    self.moveLeft = false
    
    if self.deltaTimeAnims < 1 then
      AnimationCheck(1, self.moveLeft)
    elseif self.deltaTimeAnims > 1 then
      AnimationCheck(2, self.moveLeft)
    end
    self.body:applyForce(512, 0)
  end
  if(love.keyboard.isDown("left")) then
    self.moveLeft = true
    if self.deltaTimeAnims < 1 then
      AnimationCheck(1, self.moveLeft)
    elseif self.deltaTimeAnims > 1 then
      AnimationCheck(2, self.moveLeft)
    end
    self.body:applyForce(-512, 0)
  end
  
  self:ClampVelocity(500, 1000)
end

function Player:Draw()
  love.graphics.setColor(gRed, gGreen, gBlue)
  local sprite = GetHeadSprite(gHeadNum * 10)
  local x, y, w, h = sprite:getViewport();
  x, y = self.body:getPosition()
  love.graphics.draw(gSpriteSheet, sprite, x, y-35, 0, 1, 1, w/2, h/2)
  sprite = GetBodySprite(gBodyNum* 10)
  x, y, w, h = sprite:getViewport();
  x, y = self.body:getPosition()
  love.graphics.draw(gSpriteSheet, sprite, x, y, 0, 1, 1, w/2, h/2)
  sprite = GetLegSprite(gLegNum * 10)
  x, y, w, h = sprite:getViewport();
  x, y = self.body:getPosition()
  love.graphics.draw(gSpriteSheet, sprite, x, y+35, 0, 1, 1, w/2, h/2)
end
