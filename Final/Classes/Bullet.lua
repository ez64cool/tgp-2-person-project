Bullet = GameObject:new()

function Bullet:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Bullet:CreateBody(x, y, bodyType)
  self.body = love.physics.newBody(world, x or 0, y or 0, bodyType or "static")
  self.body:setGravityScale(0)
end

function Bullet:UpdateFixture()
  self.fixture = love.physics.newFixture(self.body, self.shape)
  self.fixture:setSensor(true)
end