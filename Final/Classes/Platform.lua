Platform = GameObject:new()

function Platform:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Platform:InitPlatform(x, y, w, h)
  self:Init(x, y, w, h, 2)
  
  for i = 1, table.getn(worldSetup.platforms), 1 do
    local p = worldSetup.platforms[i]
    local x1, y1 = self.body:getPosition()
    local w1, h1 = self.shape:getPoints()
    
    local x2, y2 = p.body:getPosition()
    local w2, h2 = p.shape:getPoints()
    
    if(p ~= self and CheckCollision(x1,y1,w1*-2,h1*-2, x2,y2,w2*-2,h2*-2)) then
      self.body:setY(-worldSetup.offsetY - math.random(0, 500))
      self.body:setX(math.random(100, 1100))
      
      i = 1
    end
  end
end

function Platform:Update(dt)
  if(self.body:getY() > 720 + self.height - worldSetup.offsetY) then
      self.body:setY(-worldSetup.offsetY - math.random(0, 500))
      self.body:setX(math.random(100, 1100))
      for i = 1, table.getn(worldSetup.platforms), 1 do
        local p = worldSetup.platforms[i]
        local x1, y1 = self.body:getPosition()
        local w1, h1 = self.shape:getPoints()
        
        local x2, y2 = p.body:getPosition()
        local w2, h2 = p.shape:getPoints()
        
        if(p ~= self and CheckCollision(x1,y1,w1*-2,h1*-2, x2,y2,w2*-2,h2*-2)) then
          self.body:setY(-worldSetup.offsetY - math.random(0, 500))
          self.body:setX(math.random(100, 1100))
          
          i = 1
        end
      end
    end
end