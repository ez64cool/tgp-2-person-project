require 'Classes/GameObject'
require 'Classes/player'
require 'Classes/SpinEnemy'
require 'Classes/Turret'
require 'Classes/Platform'

local backgroundImage = {};
local backgroundOffset = 0;

worldSetup = {}

spriteSheet = {}

function WorldLoad()
  math.randomseed(love.timer.getTime())
  
  spriteSheet = love.graphics.newImage("Images/Platform.png")
  
  love.physics.setMeter(64)
  world = love.physics.newWorld(0, 9.81 * 64, true)
  
  
  world:setCallbacks(BeginContact, EndContact, PreSolve, PostSolve)
  
  worldSetup = {}
  worldSetup.offsetY = 0
  
  worldSetup.ground = {}
  worldSetup.ground.body = love.physics.newBody(world, 1280/2, 720-50/2)
  worldSetup.ground.shape = love.physics.newRectangleShape(1280, 50)
  worldSetup.ground.fixture = love.physics.newFixture(worldSetup.ground.body, worldSetup.ground.shape)
  worldSetup.ground.fixture:setFriction(0.5)
  
  worldSetup.wallL = {}
  worldSetup.wallL.body = love.physics.newBody(world, 1280-50/2, 720/2)
  worldSetup.wallL.shape = love.physics.newRectangleShape(50, 720)
  worldSetup.wallL.fixture = love.physics.newFixture(worldSetup.wallL.body, worldSetup.wallL.shape)
  
  worldSetup.wallR = {}
  worldSetup.wallR.body = love.physics.newBody(world, 50/2, 720/2)
  worldSetup.wallR.shape = love.physics.newRectangleShape(50, 720)
  worldSetup.wallR.fixture = love.physics.newFixture(worldSetup.wallR.body, worldSetup.wallR.shape)
  
  worldSetup.player = Player:new()
  worldSetup.player:Init(window.width / 2, window.height - 100, 50, 100, 1, "dynamic")
  worldSetup.player:InitPlayer()
  
  worldSetup.enemy = SpinEnemy:new()
  worldSetup.enemy:InitSpinEnemy(500, 0,  50, 100)
  
  worldSetup.turret = Turret:new()
  worldSetup.turret:InitTurret(50, 100, 50, 5)
  
  worldSetup.platforms = {}
  local plat = Platform:new()
  plat:InitPlatform(640, 360, 200, 50)
  plat:InitSprites(0,0, 200, 50, 1)
  table.insert(worldSetup.platforms, plat)
  for i=0, 5 do
    local temp = Platform:new()
    temp:InitPlatform(math.random(200, 500), math.random(500, -500), 200, 50)
    temp:InitSprites(0,0, 200, 50, 1)
    table.insert(worldSetup.platforms, temp)
  end
  
  backgroundImage = love.graphics.newImage("Images/background.png");
end

function BeginContact(a, b, coll)
  if(a == worldSetup.player.fixture) then
    worldSetup.player:BeginContact(b, coll)
  elseif(b == worldSetup.player.fixture) then
    worldSetup.player:BeginContact(a, coll)
  end
end

function EndContact(a, b, coll)
  if(a == worldSetup.player.fixture) then
    worldSetup.player:EndContact(b, coll)
  elseif(b == worldSetup.player.fixture) then
    worldSetup.player:EndContact(a, coll)
  end
end

function PreSolve(a, b, coll)
end

function PostSolve(a, b, coll, normalImpulse, tangentImpulse)
  
end

function WorldKeyPressed(key)
  worldSetup.player:keypressed(key)
end

function WorldUpdate(dt)
  world:update(dt)
  
  worldSetup.player:Update(dt)
  
  worldSetup.enemy:Update(dt)
  worldSetup.turret:Update(dt)
  
  local playerOffsetY = worldSetup.player.body:getY()
  
  if(playerOffsetY >= 720 - worldSetup.offsetY) then
    --Game Over
  end
  
  if(playerOffsetY <= 720/2 - worldSetup.offsetY) then
    backgroundOffset = 720/2 - (playerOffsetY)/10
    worldSetup.offsetY = 720/2 - playerOffsetY
    worldSetup.wallR.body:setY(playerOffsetY)
    worldSetup.wallL.body:setY(playerOffsetY)
  end
  
  for i, platform in ipairs(worldSetup.platforms) do
    platform:Update(dt)
  end
end

function WorldDraw()
  love.graphics.setColor(255,255,255)
  
  local dump, backgroundHeight = backgroundImage:getDimensions()
  
  while(backgroundOffset > backgroundHeight) do
    backgroundOffset = backgroundOffset - backgroundHeight
  end
  
  love.graphics.draw(backgroundImage, 0, backgroundOffset - backgroundHeight)
  love.graphics.draw(backgroundImage, 0, backgroundOffset)
  
  love.graphics.translate(0, worldSetup.offsetY)
  
  love.graphics.setColor(255, 155, 155, 255)
  love.graphics.polygon("fill", worldSetup.ground.body:getWorldPoints(worldSetup.ground.shape:getPoints()))
  
  love.graphics.setColor(155, 155, 255, 255)
  love.graphics.polygon("fill", worldSetup.wallL.body:getWorldPoints(worldSetup.wallL.shape:getPoints()))
  
  love.graphics.setColor(155, 155, 155, 255)
  love.graphics.polygon("fill", worldSetup.wallR.body:getWorldPoints(worldSetup.wallR.shape:getPoints()))
  
  for i, platform in ipairs(worldSetup.platforms) do
    platform:Draw()
  end
  
  worldSetup.enemy:Draw()
  worldSetup.turret:Draw()
  
  worldSetup.player:Draw()
end