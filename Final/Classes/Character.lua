require 'Classes/GameObject'

Character = GameObject:new{ 
  health = 10,
  velocityX = 0,
  velocityY = 0,
  falling = false,
}

function Character:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Character:ClampVelocity(x, y)
  local vx, vy = self.body:getLinearVelocity()
  
  if(vx > x) then
    vx = x
  elseif(vx < -x) then
    vx = -x
  end
  
  if(vy > y) then
    vy = y
  elseif(vy < -y) then
    vy = -y
  end
  
  self.body:setLinearVelocity(vx, vy)
end

function Character:Draw()
  love.graphics.setColor(72, 160, 14)
  love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints()))
end
