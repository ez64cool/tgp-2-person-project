GameObject = {
  body = {},
  shape = {},
  fixture = {},
  frames = {},
  frameCount = 0,
  height = 0,
  width = 0
}

function GameObject:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function GameObject:Init(x, y, w, h, category, bodyType)
  self:CreateBody(x, y, bodyType)
  self.body:setFixedRotation(true)
  self:CreateShape(w, h)
  self:UpdateFixture()
  self.fixture:setCategory(category or 16)
  self.width = w;
  self.height = h;
end

function GameObject:InitSprites(x, y, w, h, frameCount)
  self.frameCount = frameCount
  table.insert(self.frames, love.graphics.newQuad(x, y, w, h, spriteSheet:getDimensions()))
end

function GameObject:CreateBody(x, y, bodyType)
  self.body = love.physics.newBody(world, x or 0, y or 0, bodyType or "static")
end

function GameObject:CreateShape(w, h)
  self.shape = love.physics.newRectangleShape(w or 1, h or 1)
end

function GameObject:UpdateFixture()
  self.fixture = love.physics.newFixture(self.body, self.shape)
end

function GameObject:BeginContact(other, coll)
  
end

function GameObject:EndContact(other, coll)
  
end

function GameObject:PreSolve(other, coll)
  
end

function GameObject:PostSolve(other, coll, normalImpulse, tangentImpulse)
  
end

function GameObject:Update(dt)
  
end

function GameObject:Draw()
  love.graphics.setColor(72, 160, 14)
  if(self.frameCount > 0) then
    love.graphics.setColor(255,255,255)
    local x, y, w, h = self.frames[1]:getViewport();
    x, y = self.body:getPosition()
    love.graphics.draw(spriteSheet, self.frames[1], x, y, 0, 1, 1, w/2, h/2)
  else
    love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints()))
  end
end