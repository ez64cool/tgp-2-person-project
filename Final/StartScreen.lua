require "characterCreation"
require 'Globals'
local ButtonMaker = require ("Button")
require "Slider"

local deltaTimeAnims = 0

local Button1 = {}
local Button2 = {}
local Button3 = {}
local Button4 = {}
local Button5 = {}
local Button6 = {}
local Button7 = {}

local sprites= {}
local headNumber
local bodyNumber
local legNumber
local currentPart
local colours = {}

local done = false

local moveLeft = false
colours.a = {}

function startSceenLoad(arg)
  
  sprites.player = love.graphics.newImage("Sprites/fullSprites.png")
  sprites.character = love.graphics.newImage("Sprites/InGameSprites(51pix-distance).png")
  
  CreateCharacter(sprites.player, 9, 400, 800) --Seprates the player heads
  SheetToSprite(sprites.character, 50, 100, 51, 16, 5)
  gSpriteSheet = sprites.character
  
  headNumber = 0
  bodyNumber = 0
  legNumber = 0
  currentPart = 0 -- sets the part of the character that will change when the correct input is placed
  
  --The Colour values of the player
  colours.r = 255
  colours.g = 255
  colours.b = 255
  colours.a= 255

  
  Button1 = ButtonMaker.create(25, 50, 64, 32, "-", ButtonMaker )
  Button2 = ButtonMaker.create(550, 50, 64, 32, "+", ButtonMaker)
  Button3 = ButtonMaker.create(25, 350, 64, 32, "-", ButtonMaker )
  Button4 = ButtonMaker.create(550, 350, 64, 32, "+", ButtonMaker)
  Button5 = ButtonMaker.create(25, 650, 64, 32, "-", ButtonMaker )
  Button6 = ButtonMaker.create(550, 650, 64, 32, "+", ButtonMaker)
  Button7 = ButtonMaker.create(1000, 500, 64, 32, "Done", ButtonMaker)
  
  createSlider(700, 600)
  
  testV = "nothing-atm"
  
  
end

function startSceenUpdate(dt)
  
    testV =  dt   
  deltaTimeAnims = deltaTimeAnims + (dt * 10)
  if deltaTimeAnims > 2 then
    deltaTimeAnims = 0
  end
  
  if headNumber > 7 then
    headNumber = 0
    
  elseif headNumber < 0 then
    headNumber = 7
  end
  
  if bodyNumber > 8 then
    bodyNumber = 0
    
  elseif bodyNumber < 0 then
    bodyNumber = 7
  end
  
  if legNumber > 7 then
    legNumber = 0
    
  elseif legNumber < 0 then
    legNumber = 7
  end
  
  Button1:update()
  Button2:update()
  Button3:update()
  Button4:update()
  Button5:update()
  Button6:update()
  Button7:update()
  
  --movement animations
  if love.keyboard.isDown('a') then --move left
    moveLeft = true
    
    if deltaTimeAnims < 1 then
    AnimationCheck(1, moveLeft)
  elseif deltaTimeAnims > 1 then
    AnimationCheck(2, moveLeft)
    end
    
  elseif love.keyboard.isDown('d') then --move right
    moveLeft = false
    
  if deltaTimeAnims < 1 then
    AnimationCheck(1, moveLeft)
  elseif deltaTimeAnims > 1 then
    AnimationCheck(2, moveLeft)
  end
elseif love.keyboard.isDown('w') then --move left
  AnimationCheck(4, moveLeft)
  
  else
    AnimationCheck(0, moveLeft)

  end
  
end

function startSceenDraw()
  
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())

  love.graphics.setColor(colours.r, colours.g, colours.b, colours.a)
  love.graphics.draw(sprites.player, GetHead(headNumber),100, 0)
  love.graphics.draw(sprites.player, GetBody(bodyNumber),100, 200)
  love.graphics.draw(sprites.player, GetLegs(legNumber),100, 600)
  love.graphics.draw(sprites.character, GetHeadSprite(headNumber*10),1000, 100)
  love.graphics.draw(sprites.character, GetBodySprite(bodyNumber*10),1000, 125)
  love.graphics.draw(sprites.character, GetLegSprite(legNumber*10),1000, 175)
  
  Button1:draw()
  Button2:draw()
  Button3:draw()
  Button4:draw()
  Button5:draw()
  Button6:draw()
  Button7:draw()
  
  drawSlider()
  love.graphics.print("FPS: " .. love.timer.getFPS() .. " current mouse button = " .. testV, 700, 25)
end


function startScreenMousepressed(x, y, button, istouch)
  

 

  if Button1:mousepressed(x, y, button) then
    headNumber = headNumber - 1
      
  end
  
    if Button2:mousepressed(x, y, button) then
    headNumber = headNumber + 1
  end
  
  if Button3:mousepressed(x, y, button) then
    bodyNumber = bodyNumber - 1
      
  end
  
  if Button4:mousepressed(x, y, button) then
    bodyNumber = bodyNumber + 1
  end
  
  if Button5:mousepressed(x, y, button) then
    legNumber = legNumber - 1
      
  end
  
  if Button6:mousepressed(x, y, button) then
    legNumber = legNumber + 1
  end
  
  if Button7:mousepressed(x, y, button) then
    done = true
  end
  
colours.r = updateSliderPosRed(x,y)
colours.g = updateSliderPosGreen(x,y)
colours.b = updateSliderPosBlue(x,y) 
 
end

function IsStartDone()
  if(done)then
    gHeadNum = headNumber
    gBodyNum = bodyNumber
    gLegNum = legNumber
    gRed = colours.r
    gGreen = colours.g
    gBlue = colours.b
  end
  return done
end