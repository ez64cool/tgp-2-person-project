local Character = {}
Character.head = {}
Character.body = {}
Character.legs = {}

Sprite = {}
Sprite.heads = {}
Sprite.bodies = {}
Sprite.legs = {}

local animNumb = 1

local CalcX
local CalcY
local spriteCount

function CreateCharacter(spriteSheet, frameTotal, spriteX, spriteY)
  
  for i = 0, frameTotal do -- creates the rects for the head of the character
    XPointCalc = spriteX* i --calculates the distence between each sprite
    Character.head[i] = love.graphics.newQuad(XPointCalc, 0, spriteX, (spriteY / 4), spriteSheet:getDimensions())
  end
  
  for i = 0, frameTotal do
    XPointCalc = spriteX* i
    Character.body[i] = love.graphics.newQuad(XPointCalc, (spriteY / 4), spriteX, (spriteY / 2), spriteSheet:getDimensions())
  end
  
  for i = 0, frameTotal do
    XPointCalc = spriteX* i
    Character.legs[i] = love.graphics.newQuad(XPointCalc, (spriteY - (spriteY / 4)), spriteX, (spriteY / 4), spriteSheet:getDimensions())
  end
end

function GetHead(frameNumb)
  
  return Character.head[frameNumb]
  
end

function GetBody(frameNumb)
  
  return Character.body[frameNumb]
  
end

function GetLegs(frameNumb)
  
  return Character.legs[frameNumb]
  
end

function SheetToSprite(spriteSheet,spriteWidth,spriteHeight, spriteGap, spriteColNumb, spriteRowNumb)

spriteCount = 0

  for i = 0, spriteColNumb do
    CalcY = spriteHeight * i
    
    for j = 0, spriteRowNumb do
      CalcX = (spriteWidth* j) + (spriteGap* j )
      Sprite.heads[j + spriteCount] = love.graphics.newQuad(CalcX, CalcY, spriteWidth, (spriteHeight / 4), spriteSheet:getDimensions())
      Sprite.bodies[j + spriteCount] = love.graphics.newQuad(CalcX, CalcY + (spriteHeight / 4), spriteWidth, (spriteHeight / 2), spriteSheet:getDimensions())
      Sprite.legs[j + spriteCount] = love.graphics.newQuad(CalcX, CalcY + ((spriteHeight / 4) + (spriteHeight /2)), spriteWidth, (spriteHeight / 4), spriteSheet:getDimensions())
    end
spriteCount = spriteCount + spriteRowNumb
    
  end
    
end

function GetHeadSprite(frameNumb)
  
  return Sprite.heads[frameNumb + animNumb]
  
end

function GetBodySprite(frameNumb)
  
  return Sprite.bodies[frameNumb + animNumb]
  
end

function GetLegSprite(frameNumb)
  
  return Sprite.legs[frameNumb + animNumb]
  
end

function AnimationCheck(animAddition, isLeft)
  
  if isLeft == true then
    animNumb = animAddition + 5
  else
    animNumb = animAddition
  end
  
end
