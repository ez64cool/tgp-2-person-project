
local barPositionRed = {}
local sliderPositionRed = {}
local barPositionGreen = {}
local sliderPositionGreen = {}
local barPositionBlue = {}
local sliderPositionBlue = {}

function createSlider(x,y)
  
  
  --set red bar
  barPositionRed.x = x
  barPositionRed.y = y
  barPositionRed.w = 255
  barPositionRed.h = 20
  sliderPositionRed.x = x + 255
  sliderPositionRed.y = y
  sliderPositionRed.w = 5
  sliderPositionRed.h = 18
  
  --set green bar
  barPositionGreen.x = x 
  barPositionGreen.y = y + 25
  barPositionGreen.w = 255
  barPositionGreen.h = 20
  sliderPositionGreen.x = x + 255
  sliderPositionGreen.y = y + 25
  sliderPositionGreen.w = 5
  sliderPositionGreen.h = 18
  
  --set blue bar
  barPositionBlue.x = x 
  barPositionBlue.y = y + 50
  barPositionBlue.w = 255
  barPositionBlue.h = 20
  sliderPositionBlue.x = x + 255
  sliderPositionBlue.y = y + 50
  sliderPositionBlue.w = 5
  sliderPositionBlue.h = 18 

end

function drawSlider()
  
  --Draw Red bar
  love.graphics.setColor(255, 0, 0, 255)
  love.graphics.rectangle('fill', barPositionRed.x, barPositionRed.y, barPositionRed.w, barPositionRed.h)
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('line', barPositionRed.x, barPositionRed.y, barPositionRed.w, barPositionRed.h)
  love.graphics.rectangle('fill', sliderPositionRed.x, sliderPositionRed.y, sliderPositionRed.w, sliderPositionRed.h)
  
  --Draw Blue bar
  love.graphics.setColor(0, 255, 0, 255)
  love.graphics.rectangle('fill', barPositionGreen.x, barPositionGreen.y, barPositionGreen.w, barPositionGreen.h)
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('line', barPositionGreen.x, barPositionGreen.y, barPositionGreen.w, barPositionGreen.h)
  love.graphics.rectangle('fill', sliderPositionGreen.x, sliderPositionGreen.y, sliderPositionGreen.w, sliderPositionGreen.h)
  
  --Draw Blue bar
  love.graphics.setColor(0, 0, 255, 255)
  love.graphics.rectangle('fill', barPositionBlue.x, barPositionBlue.y, barPositionBlue.w, barPositionBlue.h)
  love.graphics.setColor(0, 0, 0, 255)
  love.graphics.rectangle('line', barPositionBlue.x, barPositionBlue.y, barPositionBlue.w, barPositionBlue .h)
  love.graphics.rectangle('fill', sliderPositionBlue.x, sliderPositionBlue.y, sliderPositionBlue.w, sliderPositionBlue.h)
end

function updateSliderPosRed(x,y)

  if barPositionRed.x <= x and (barPositionRed.x + barPositionRed.w) >= x and
  barPositionRed.y <= y and (barPositionRed.y + barPositionRed.h) >= y then
  
    sliderPositionRed.x = x
    return sliderPositionRed.x - barPositionRed.x
  else
    return sliderPositionRed.x - barPositionRed.x
  end

end


function updateSliderPosGreen(x,y)

  if barPositionGreen.x <= x and (barPositionGreen.x + barPositionGreen.w) >= x and
  barPositionGreen.y <= y and (barPositionGreen.y + barPositionGreen.h) >= y then
  
    sliderPositionGreen.x = x
    return sliderPositionGreen.x - barPositionGreen.x
  else
    return sliderPositionGreen.x - barPositionGreen.x
  end

end


function updateSliderPosBlue(x,y)

  if barPositionBlue.x <= x and (barPositionBlue.x + barPositionBlue.w) >= x and
  barPositionBlue.y <= y and (barPositionBlue.y + barPositionBlue.h) >= y then
  
    sliderPositionBlue.x = x
    return sliderPositionBlue.x - barPositionBlue.x
  else
    return sliderPositionBlue.x - barPositionBlue.x
  end

end