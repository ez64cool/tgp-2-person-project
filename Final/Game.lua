require 'Classes/world'


function GameLoad(arg)
  WorldLoad()
end

function GameKeypressed(key)
  WorldKeyPressed(key)
end

function GameUpdate(dt)
  WorldUpdate(dt)
end

function GameDraw()
  WorldDraw()
  
  love.graphics.origin()
  
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.print("FPS: " .. love.timer.getFPS(), 25, 25)
  
  love.graphics.setColor(0, 255, 0, 255)
  love.graphics.print("Score: " .. worldSetup.offsetY)
end