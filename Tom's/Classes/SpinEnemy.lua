require 'Classes/Character'

SpinEnemy = Character:new()

function SpinEnemy:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function SpinEnemy:InitSpinEnemy(x, y, w, h)
  self:Init(x, y, w, h, 3, "dynamic")
  
  self.body:setFixedRotation(false)
end

function SpinEnemy:Update(dt)
  self.body:applyTorque(160000)
  
  if(self.body:getY() > worldSetup.offsetY + 1080) then
    self.body:setPosition(500, -worldSetup.offsetY)
    print("Reset")
  end
  
  self:ClampVelocity(1000, 1000)
end