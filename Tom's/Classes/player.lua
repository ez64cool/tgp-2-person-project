require 'Classes/Character'

Player = Character:new
{
  ableToJump = true;
}

function Player:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Player:keypressed(key)
  if(key == "up" and self.ableToJump) then
    self.body:applyLinearImpulse(0, -1000)
    self.ableToJump = false;
  end
end

function Player:BeginContact(other, coll)
  local nx, ny = coll:getNormal()
  
  if(nx ~= 0) then
    self.body:applyLinearImpulse(nx * 1000, 0)
  end
  
  self.ableToJump = true;
  
  print("Normal X: " .. nx .. " Y: " .. ny)
end

function Player:EndContact(other, coll)
end

function Player:Update(dt)
  if(love.keyboard.isDown("right")) then
    self.body:applyForce(512, 0)
  end
  if(love.keyboard.isDown("left")) then
    self.body:applyForce(-512, 0)
  end
  
  self:ClampVelocity(500, 1000)
end