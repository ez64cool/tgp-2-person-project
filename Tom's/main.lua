require 'Classes/world'


function love.load(arg)
  if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
  
  io.stdout:setvbuf("no")
  
  WorldLoad()
end

function love.keypressed(key)
  WorldKeyPressed(key)
end

function love.update(dt)
  WorldUpdate(dt)
end

function love.draw()
  WorldDraw()
  
  love.graphics.origin()
  
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.print("FPS: " .. love.timer.getFPS(), 25, 25)
  
  love.graphics.setColor(0, 255, 0, 255)
  love.graphics.print("Score: " .. worldSetup.offsetY)
end