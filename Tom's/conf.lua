function love.conf(c)
  c.title = ""
  window = c.screen or c.window
  window.width = 1280
  window.height = 720
  window.vsync = true
end

function lerp(a, b, factor)
  return a+(b-a)*factor
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end